#!/busybox/sh

/kaniko/executor -f `pwd`/Dockerfile.run -c `pwd` --cache=true --destination=$2/$2/$3
