#! /bin/bash

#go get github.com/stretchr/testify/assert
#go mod init github.com/stretchr/testify/assert
#go test main_test.go
#export PATH=$PATH:/Users/andreascaternberg/go/bin
go mod init gotest.tools/gotestsum
ls -ltr
pwd
go list
ls -la ~/
go test | gotestsum --format testname --junitfile unit-tests.xml
go test -cover
