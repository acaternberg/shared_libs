library 'bitbucket.org-acaternberg-shared_libs' _
//Resources SonarQube
def sonarQubePod = libraryResource 'podtemplates/podtemplate-qs-sonarqube.yaml'

pipeline {

    agent {
        kubernetes {
            yaml sonarQubePod
        }
    }
    stages {
        // Not required to clone yet
       /* stage('Checkout') {
            steps {
                git branch: 'main' , credentialsId: 'bbssh' , url: 'git@bitbucket.org:acaternberg/app_go1.git'
            }
        }*/

        stage('QS') {
            options {
                skipDefaultCheckout(true)
            }
            steps {
                git branch: 'main' , credentialsId: 'bbssh' , url: 'git@bitbucket.org:acaternberg/app_go1.git'
                container('sonarscanner') {
                    withSonarQubeEnv("sonarqube") {
                        sh "ls -l"

                        sh "sonar-scanner -Dproject.settings=./sonar-project.properties"
                    }
                }
            }
        }
    }

    post {
        //see https://www.jenkins.io/doc/book/pipeline/syntax/#post
        always {
            echo "Run the steps in the post section regardless of the completion status of the Pipeline’s or stage’s run.\n"
        }
        success {
            echo "Only run the steps in post if the current Pipeline’s or stage’s run has a \"success\" status, typically denoted by blue or green in the web UI.\n"
        }
        failure {
            echo "Only run the steps in post if the current Pipeline’s or stage’s run has a \"failed\" status, typically denoted by red in the web UI.\n"
        }
    }
}