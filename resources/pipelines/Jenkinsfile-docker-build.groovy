library 'bitbucket.org-acaternberg-shared_libs' _
//Resources for CI
def kanikoPod = libraryResource 'podtemplates/podtemplate-kaniko.yaml'

pipeline {
    parameters {
        choice choices: ['k8s','golang-ci'], description: 'shared_libs/resources/docker/$DOCKER_DIR/Dockerfile', name: 'DOCKER_DIR'
        choice choices: ['caternberg/jenkins-agent-k8s', 'caternberg/golang-customized'], description: '''Where to push the docker image
kaniko executor .....--destination ${params.REGISTRY_REPOSITORY_IMAGE}:BUILD_NUMBER-${BUILD_NUMBER}''', name: 'REGISTRY_REPOSITORY_IMAGE'
    }

    environment {
        PATH = "/busybox:$PATH"
        DOCKER_DIR = "${params.DOCKER_DIR}"
        REGISTRY_REPOSITORY_IMAGE = "${params.REGISTRY_REPOSITORY_IMAGE}"
    }
    agent {
        kubernetes {
            defaultContainer 'kaniko'
            yaml kanikoPod
        }
    }
    stages {
        stage('Build with Kaniko') {
            steps {
                // git branch: 'main', changelog: false, credentialsId: 'github-user-ssh', poll: false, url: 'git@github.com:cccaternberg/cloudbees-ci-config.git'
                sh 'ls -lR'
                container(name: 'kaniko', shell: '/busybox/sh') {
                    echo "/kaniko/executor ${DOCKER_DIR}/Dockerfile  --destination ${REGISTRY_REPOSITORY_IMAGE}:BUILD_NUMBER-${BUILD_NUMBER}"
                    sh '''#!/busybox/sh
                    /kaniko/executor  --dockerfile $(pwd)/resources/docker/${DOCKER_DIR}/Dockerfile --insecure --skip-tls-verify --cache=false  --context $(pwd) --destination ${REGISTRY_REPOSITORY_IMAGE}:BUILD_NUMBER-${BUILD_NUMBER}
                '''
                }
            }
        }
    }
}
