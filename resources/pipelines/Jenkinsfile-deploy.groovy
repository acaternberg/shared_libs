library 'bitbucket.org-acaternberg-shared_libs' _
def podtemplateOS = libraryResource 'podtemplates/podtemplate-os-tools.yaml'
def kustomizedeploy = "kustomize.sh"
def k8sendpoint = "https://34.73.254.21"
def namespace = "pr"
def credentialsID = "botsa"
//Not used
def apiScript = libraryResource 'scripts/serviceAccountAPItest.sh'

pipeline {
    agent {
        kubernetes {
            yaml podtemplateOS
        }
    }

    stages {

        stage('Checkout') {
            steps {
                git branch: 'master', changelog: false, credentialsId: 'github-user-ssh', poll: false, url: 'git@github.com:org-caternberg/helloworld.git'
            }
        }

        stage('Component Stage') {
            steps {
                container('custom-agent') {
                    echo 'TODO:orchestration phase.....'
                }
            }
        }

        stage('Deploy') {
            when { expression { return fileExists('kustomize.sh') } }
            steps {
                echo "file exists"
                container('custom-agent') {
                    echo 'Hello World!'
                    withKubeConfig(credentialsId: "${credentialsID}", namespace: "${namespace}", serverUrl: "${k8sendpoint}") {
//                        sh "/usr/local/bin/kubectl version"
//                        sh "kubectl delete pod appgo -ns pr"
//                        sh "kubectl run appgo --image=caternberg/helloworldgolang -n pr --port=8080"
//                        sh "sleep 10"
//                        sh "/usr/local/bin/kubectl get all"
                        timeout(time: 3, unit: 'MINUTES') {
                            retry(0) {
                                sh "${kustomizedeploy}"
                            }
                        }
                    }
                }
            }
        }
        /*
         stage('ClusterAdmin') {
             steps {
                 container('custom-agent') {
                     echo 'Hello World!'
                     withKubeConfig(credentialsId: '558e0a99-4f2b-4e1e-9847-44ad3df7d9cc', namespace: 'cloudbees-core', serverUrl: 'https://34.73.254.21/') {
                         sh "kubectl version"
                         // sh "kubectl ${params.kubectl_command}"
                         timeout(time: 3, unit: 'MINUTES') {
                             retry(5) {
                                 //call external shell script
                                 sh "${apiScript}"
                             }
                         }
                     }
                 }
             }
         }*/

    }
}
