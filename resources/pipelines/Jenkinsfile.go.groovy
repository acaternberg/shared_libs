library 'bitbucket.org-acaternberg-shared_libs' _
//Resources for CI
def golang = libraryResource 'podtemplates/podtemplate-golang.yaml'
def goBuildSH = libraryResource 'scripts/build-go-bin.sh'
def goTestSH = libraryResource 'scripts/test-go.sh'
def kanikoSH = libraryResource 'scripts/kaniko.sh'

//Resources for CD
def ostools = libraryResource 'podtemplates/podtemplate-os-tools.yaml'
def deploy = "k8sdeploy.sh"
def k8sendpoint = "https://34.73.254.21"
def namespace = "pr"
def credentialsID = "botsa"
//Not used
def apiScript = libraryResource 'scripts/serviceAccountAPItest.sh'

pipeline {
    environment {
        REGISTRY = 'index.docker.io' // Configure your own registry
        REPOSITORY = 'caternberg'
        IMAGE = "${REGISTRY}/${REPOSITORY}/helloworldgolang-${BUILD_NUMBER}-${env.GIT_COMMIT.take(7)}"
    }
    agent {
        kubernetes {
            yaml golang
            //Load from local path
            //yamlFile 'resources/podtemplates/podtemplate-golang.yaml'
        }
    }
    stages {
        /* Not required to clone yet
        stage('Checkout') {
             steps {
                 git branch: 'master', changelog: false, credentialsId: 'github-user-ssh', poll: false, url: 'git@github.com:org-caternberg/helloworld.git'
             }
         }
         */
        stage('Build') {
            options {
                skipDefaultCheckout(true)
            }
            steps {
                container('golang') {
                    timeout(time: 3, unit: 'MINUTES') {
                        retry(5) {
                            echo "GIT: ${GIT_COMMIT}"
                            sh "${goBuildSH}"
                            archiveArtifacts '**/*.go'
                        }
                    }
                }
            }
        }

        stage('Test') {
            when { branch pattern: "feature.*", comparator: "REGEXP" }
            options {
                skipDefaultCheckout(true)
            }
            steps {
                container('golang') {
                    timeout(time: 3, unit: 'MINUTES') {
                        retry(5) {
                            sh "${goTestSH}"
                            junit '**/unit-tests.xml'
                            recordIssues(tools: [junitParser(pattern: 'unit-tests.xml', reportEncoding: 'UTF-8')])
                            //publishIssues issues: [], name: 'issues', qualityGates: [[threshold: 1, type: 'TOTAL', unstable: false]], sourceCodeEncoding: 'UTF-8'
                            publishChecks name: 'checks', status: 'IN_PROGRESS', summary: 'test done', text: 'test check', title: 'test check'
                        }
                    }
                }
            }
        }
        stage('Make Image') {
            //when { branch pattern: "feature.*", comparator: "REGEXP" }
            options {
                skipDefaultCheckout(true)
            }
            environment {
                PATH = "/busybox:$PATH"
            }
            steps {
                container(name: 'kaniko', shell: '/busybox/sh') {
                    sh '''#!/busybox/sh
                    /kaniko/executor -f $(pwd)/Dockerfile.run -c $(pwd) --cache=true --destination=${IMAGE}
                    '''
                }
            }
            post {
                //see https://www.jenkins.io/doc/book/pipeline/syntax/#post
                success {
                    echo "Successfully kaniko push to ${IMAGE}"
                }
                failure {
                    echo "Failed push to ${IMAGE}"
                }
            }
        }
        stage('Deploy') {
            options {
                skipDefaultCheckout(false)
            }
            agent {
                kubernetes {
                    yaml ostools
                }
            }
            //when { expression { return fileExists("${deploy}") }
//            when {
//                allOf {
//                    expression { return fileExists("${deploy}") }
//                    branch pattern: "PR.*", comparator: "REGEXP"
//                }
//                beforeAgent true
//            }
            steps {
                container('k8s') {
                    withKubeConfig(credentialsId: "${credentialsID}", namespace: "${namespace}", serverUrl: "${k8sendpoint}") {
//                        sh "/usr/local/bin/kubectl version"
//                        sh "kubectl delete pod appgo -ns pr"
//                        sh "kubectl run appgo --image=caternberg/helloworldgolang -n pr --port=8080"
//                        sh "sleep 10"
//                        sh "/usr/local/bin/kubectl get all"
                        timeout(time: 3, unit: 'MINUTES') {
                            retry(1) {
                                sh "./${deploy} ${IMAGE}"
                            }
                        }
                    }
                }
            }
        }
    }

    post {
        //see https://www.jenkins.io/doc/book/pipeline/syntax/#post
        always {
            echo "Run the steps in the post section regardless of the completion status of the Pipeline’s or stage’s run.\n"
        }
        success {
            echo "Only run the steps in post if the current Pipeline’s or stage’s run has a \"success\" status, typically denoted by blue or green in the web UI.\n"
        }
        failure {
            echo "Only run the steps in post if the current Pipeline’s or stage’s run has a \"failed\" status, typically denoted by red in the web UI.\n"
        }
    }
}