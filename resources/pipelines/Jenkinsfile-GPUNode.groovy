
pipeline {
    agent {
        kubernetes {
               yaml '''
apiVersion: v1
kind: Pod
metadata:
  name: gpu-pod
#  annotations:
#    k8s.io/cluster-autoscaler/node-template/label/nvidia.com/gpu: true
#    k8s.io/cluster-autoscaler/node-template/resources/nvidia.com/gpu: 1
spec:
  containers:
  - name: gpu-container
    image: nvidia/cuda:11.0.3-runtime-ubuntu20.04
    command: ["/bin/bash", "-c", "--"]
    args: ["while true; do sleep 600; done;"]
    resources:
      limits:
       nvidia.com/gpu: 1
  tolerations:
    - key: nvidia.com/gpu
      operator: Exists
      effect: "NoSchedule"
  restartPolicy: OnFailure
  nodeSelector:
    hasgpu: "true"
'''
            defaultContainer 'gpu-container'
        }
    }
    stages {
        stage('Main') {
            steps {
                sh 'hostname'
            }
        }
    }
}


