#! /bin/sh

# exit if a command fails
set -xe

LC_ALL=en_US.UTF-8
LANG=en_US.UTF-8
LANGUAGE=en_US.UTF-8
PROTOBUF_REVISION=3.15.6
GOLANGCI_LINT_VERSION=v1.39.0
ALPINE_GLIBC_BASE_URL=https://github.com/andyshinn/alpine-pkg-glibc/releases/download
ALPINE_GLIBC_PACKAGE_VERSION=2.27-r0
ALPINE_GLIBC_BASE_PACKAGE_FILENAME=glibc-$ALPINE_GLIBC_PACKAGE_VERSION.apk
ALPINE_GLIBC_BIN_PACKAGE_FILENAME=glibc-bin-$ALPINE_GLIBC_PACKAGE_VERSION.apk
ALPINE_GLIBC_I18N_PACKAGE_FILENAME=glibc-i18n-$ALPINE_GLIBC_PACKAGE_VERSION.apk

apk add --no-cache \
    alpine-sdk \
    ca-certificates \
    tzdata \
    jq \
    autoconf \
    automake \
    libstdc++ \
    libtool \
    unzip \
    wget

curl -sLO https://github.com/google/protobuf/releases/download/v${PROTOBUF_REVISION}/protoc-${PROTOBUF_REVISION}-linux-x86_64.zip \
    && unzip protoc-${PROTOBUF_REVISION}-linux-x86_64.zip -d /usr/local \
    && chmod +x /usr/local/bin/protoc \
    && chmod -R 755 /usr/local/include/ \
    && rm protoc-${PROTOBUF_REVISION}-linux-x86_64.zip

curl -sL \
        -O "$ALPINE_GLIBC_BASE_URL/$ALPINE_GLIBC_PACKAGE_VERSION/$ALPINE_GLIBC_BASE_PACKAGE_FILENAME" \
        -O "$ALPINE_GLIBC_BASE_URL/$ALPINE_GLIBC_PACKAGE_VERSION/$ALPINE_GLIBC_BIN_PACKAGE_FILENAME" \
        -O "$ALPINE_GLIBC_BASE_URL/$ALPINE_GLIBC_PACKAGE_VERSION/$ALPINE_GLIBC_I18N_PACKAGE_FILENAME" \
    && apk add --no-cache --allow-untrusted \
        "$ALPINE_GLIBC_BASE_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_BIN_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_I18N_PACKAGE_FILENAME" \
    && /usr/glibc-compat/bin/localedef --force --inputfile POSIX --charmap UTF-8 C.UTF-8 || true \
    && echo "export LANG=C.UTF-8" > /etc/profile.d/locale.sh \
    && rm "$ALPINE_GLIBC_BASE_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_BIN_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_I18N_PACKAGE_FILENAME"

go get -u -v github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway@v2.3.0
go get -u -v github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2@v2.3.0
go get -u -v google.golang.org/protobuf/cmd/protoc-gen-go@f2d1f6cbe10b90d22296ea09a7217081c2798009
go get -u -v google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.1.0

#Install gotestsum https://github.com/gotestyourself/gotestsum
go get -u -v gotest.tools/gotestsum
#go mod init gotest.tools/gotestsum
#go install gotest.tools/gotestsum

# Install golangci-lint
curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s $GOLANGCI_LINT_VERSION
go get -u -v golang.org/x/lint/golint

# golicense
git clone https://github.com/mitchellh/golicense.git /tmp/golicense
cd /tmp/golicense && go build
mv /tmp/golicense/golicense /usr/local/bin/golicense

# Clean-up packages
apk del \
    autoconf \
    automake \
    libtool \
    unzip \
    glibc-i18n \
    alpine-sdk \
    ca-certificates \
    tzdata \
    autoconf \
    automake \
    libstdc++ \
    libtool

# Install shared libs
apk add --no-cache \
    git \
    curl \
    bash \
    make \
    jq \
    yq \
    gcc \
    alpine-sdk